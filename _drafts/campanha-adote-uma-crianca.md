---
layout: post
title:  "Plante uma semente"
date:   2019-10-24
---

![imagemCrianças](/assets/images/sementes.jpeg)

O Projeto Florescer faz parte de uma Associação civil sem fins lucrativos e conta com o apoio de todos para se manter de portas abertas. Boa parte de nossos colaboradores são voluntários, entretanto, contamos também com trabalhadores que são remunerados para realizarem seu trabalho junto às nossas crianças e jovens. Além disso, mensalmente devemos quitar despesas como água, energia elétrica, lanche e material de consumo.


Por isso, lançamos a campanha Plante uma Semente. A ideia é que, colaborando com o Florescer, você estará nos ajudando a cuidar das nossas sementes, auxiliando no seu desenvolvimento e florescimento. Assim, você pode ajudar a custear o lanche, a monitoria de português ou matemática, ou o material escolar de uma criança. Com a sua ajuda, poderemos manter o Florescer funcionando e melhorar cada vez mais o Projeto.

Doe e compartilhe com seus amigos!
