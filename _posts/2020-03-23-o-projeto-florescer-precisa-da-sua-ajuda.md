---
layout: post
title:  "O Projeto Florescer precisa da sua ajuda!"
date:   2020-03-23
---

![precisamosAjuda](/assets/images/precisamosAjuda.jpeg)

Neste momento em que passamos pelo necessário isolamento social, interrompemos nossas atividades e enfrentamos dois grandes problemas:

<!--more-->

1) A quarentena no DF impede que faxineiras, pedreiros, diaristas e tantos outros trabalhadores informais tenham renda para custear suas necessidades básicas. Nosso projeto, dentro de nossas limitações, procura amenizar o sofrimento dessas famílias com a doação de alimentos.

2) A quarentena também impede que realizemos bazares e eventos que nos ajudem a custear o projeto, especialmente o salário e encargos das três dedicadas monitoras, as quais temos o dever de proteger nestes momentos de dificuldade.

Veja como ajudar no site da mantenedora: [http://casadocaminho.org.br/](http://casadocaminho.org.br/)
