---
layout: post
title:  "A importância do brincar"
date:   2019-11-24
---

![brincar](/assets/images/brincar.jpeg)


Todas as atividades de ensino são importantes, mas não podemos esquecer do papel que o brincar tem na vida das nossas crianças.

<!--more-->

Brincar é essencial para o desenvolvimento de funções cognitivas e motoras. É também uma forma de aprendizado.

Além disso, aproximar e reforçar os laços de amizade nos faz desenvolver o amor e criar cidadãos empáticos e conscientes.
