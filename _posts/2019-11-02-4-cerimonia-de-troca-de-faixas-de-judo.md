---
layout: post
title:  "4ª Cerimônia de troca de faixas de judô"
date:   2019-11-02
---

![todosJudo](/assets/images/todosJudo.jpeg)

Na última sexta-feira (1/11/2019), foi realizada a 4ª Cerimônia de troca de faixa dos alunos de judô do Projeto Florescer. O professor voluntário, Marcus Vinícius de Carvalho, destacou a importância do Judô para aumentar a disciplina e habilidades motoras das crianças.

<!--more-->

A troca de faixas representa o reconhecimento do esforço, da disciplina e do respeito desenvolvido pelos alunos durante as aulas de judô.

O evento celebrou o progresso realizado pelos nossos jovens judocas durante todo o ano. Ao todo, as dezenove crianças participantes das aulas foram promovidas.

Os pais estiveram presentes e puderam acompanhar esse momento de bastante alegria para todos.

Veja nas fotos os padrinhos escolhidos por algumas crianças para a troca das suas faixas!

![Judo 4](/assets/images/judo4.jpeg)
![Judo 1](/assets/images/judo1.jpeg)
![Judo 2](/assets/images/judo2.jpeg)
![Judo 3](/assets/images/judo3.jpeg)
![Judo 5](/assets/images/judo5.jpeg)
