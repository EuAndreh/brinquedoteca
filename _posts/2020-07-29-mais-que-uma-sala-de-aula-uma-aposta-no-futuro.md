---
layout: post
title:  "Mais que uma sala de aula, uma aposta no futuro."
date:   2020-07-29
---

![celular](/assets/images/celular.jpeg)

Aquele celular ou tablet que funciona, mas você não usa mais, pode ser útil a uma criança ou adolescente em fase escolar.


<!--more-->


 O Projeto Florescer está fazendo uma campanha de arrecadação de celulares e tablets (usados e em funcionamento) para doar aos que são atendidos pelo projeto, que estudam na rede de ensino pública no DF.

Durante a pandemia, as aulas estão sendo lecionadas a distância na plataforma Google Sala de Aula e, sem um celular ou tablet, não há possibilidade de participar das aulas online.

Muitas dessas crianças e adolescentes jamais tiveram um celular ou um tablet e acreditamos ser muito importante esse contato com a tecnologia e, por isso, estamos investindo nessa iniciativa de ensino a distância. 

Além do objetivo imediato de possibilitar a frequência escolar,  o aparelho  representará para  eles o acesso a um recurso tecnológico que já está incorporado ao cotidiano da sociedade. A impossibilidade de acesso a tais equipamentos promovem um agravamento de exclusão social.

O nosso esforço junto a elas é mostrar que o celular e o tablet não são dispositivos apenas para jogos ou redes sociais, mas também instrumento de pesquisa, de curso a distância, de capacitação profissional, de aumento da empregabilidade, de geração de renda, de descoberta do mundo, e ainda a possibilidade de fazer visitas virtuais a pontos de cultura, mundo afora. 

É a nossa aposta.


*Se você quiser participar, entre em contato pelo número 61-99223-2213 (Whatsapp) e encontraremos uma maneira buscar o equipamento que você quer doar.*
