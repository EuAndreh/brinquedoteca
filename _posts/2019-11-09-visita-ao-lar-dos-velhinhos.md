---
layout: post
title:  "Visita ao Lar dos Velhinhos"
date:   2019-11-09
---

![todosJudo](/assets/images/larVelhinhos.jpeg)


A formação de um ser humano integral, além da educação formal, deve incluir o respeito ao próximo, proatividade e senso de comunidade.

Com o objetivo de desenvolver esses aspectos, ontem, a nossa turma visitou o Lar do Velhinhos Bezerra de Menezes, em Sobradinho, DF. Foram momentos de muita animação!

<!--more-->

As crianças puderam conversar com os idosos que vivem no Lar e ouvir muitas de suas histórias.

A integração também ocorreu por meio de jogos que transcendem gerações. Quem não gosta do bom e velho jogo de damas? As crianças disputaram ainda partidas com craques do tradicional dominó!

De lá, levamos o carinho e experiência transmitida por aqueles que tanto já viveram. Esperamos ter deixado a nossa marca de alegria e respeito.

<a href="http://www.lardosvelhinhos.org.br">Conheça mais sobre o Lar do Velhinhos aqui.<a>
