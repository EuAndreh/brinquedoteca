---
layout: post
title:  "Visita do Escritório Veloso de Melo"
date:   2019-11-26
---

![velosodemelo](/assets/images/velosodemelo.jpeg)



Abraçar uma causa contribuindo financeiramente é uma das formas de ajudar as instituições e de promover uma transformação positiva na sociedade. Essa ação viabiliza a continuidade das atividades dos projetos sociais, tendo em vista que os recursos de manutenção, geralmente, são oriundos de reduzidos quadros de sócios, bazares e promoções diversas.

<!--more-->

Neste final de ano, o escritório Veloso de Melo Advogados decidiu mobilizar sua equipe para uma ação diferente: fazer o bem. Com esse intuito, após pesquisas em diversos projetos sociais, selecionaram o  Florescer e se uniram para fazer uma mega arrecadação financeira!

Hoje tivemos a felicidade de recebê-los em uma visita, quando vieram conhecer a nossa estrutura e funcionamento e nos trazer essa generosa doação.

É importante lembrar que o Florescer só funciona porque temos a doação de pessoas e instituições que acreditam no nosso projeto e objetivo.

Muito obrigado pela visita e pela doação!
