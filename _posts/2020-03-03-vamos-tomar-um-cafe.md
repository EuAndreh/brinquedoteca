---
layout: post
title:  "Vamos tomar um café?"
date:   2020-03-03
---

![eventoKoppe1](/assets/images/eventoKoppe1.jpeg)

No último domingo foi realizado o primeiro evento do Projeto Florescer.

<!--more-->

O Koppe Cafés Especiais abriu as suas para nos receber. Foram 31 convidados a tomar um café conosco na tarde do domingo.

Obrigado a todos os que compareceram e apoiaram essa ideia! Esperamos poder realizar novos eventos que, além de angariar receitas para o Florescer, reúnam e agreguem pessoas:)

Todo o lucro do evento foi revertido para o Projeto Florescer.


![eventoKoppe2](/assets/images/eventoKoppe2.jpeg)

![eventoKoppe3](/assets/images/eventoKoppe3.jpeg)
