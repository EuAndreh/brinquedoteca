---
layout: post
title:  "Judô no Ano Novo"
date:   2020-01-01
---

![judo-ano-novo](/assets/images/judoanonovo.jpeg)


1º de janeiro. Judocas de Sobradinho, professores e alunos do Projeto Florescer e da Associação Granado se reuniram nesta data em celebração ao ano que se inicia. Além de confratenizar, o objetivo do encontro foi o de reforçar a determinação de treinar e estar juntos, para que essa prática seja renovada ao longo do ano.

<!--more-->

O professor Adilson Granado, coordenador da Associação Granado, encerrou a atividade trazendo reflexões sobre a importância deste dia.

Destacou que o momento deve ser de mudança individual com propósitos de melhoria para realmente ser diferente, inclusive, em relação aos que são considerados inimigos, até aos que não foram bons conosco, aos quais se deve pedir perdão. Disse que é o momento de retratação com os que, de alguma forma, desprezamos, pois é a hora de elevar o espírito e desejar, do fundo da alma, o bem ao próximo.

Salientou que o judô não é somente para treinar, pois a equipe deve ser considerada uma segunda família, e o ambiente deve ser o de servir e estar disponível para o outro, pois é precisamente aí que está a felicidade.

O professor enfatizou, ainda, que o ano de 2020 é o ano da igualdade, 20 e 20, é a ideia que nos traz esse número: não deve haver diferenças de etnia, de religião ou de condição social. É enfim, o ano em que se deve buscar a união entre as pessoas.

Saudemos, pois, de corações abertos este ano que chega. E que venha a harmonia entre indivíduos, grupos e nações.

O Projeto Florescer deseja a todos um feliz 2020!
