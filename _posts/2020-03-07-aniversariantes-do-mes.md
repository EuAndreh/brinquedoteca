---
layout: post
title:  "Aniversariantes do mês!"
date:   2020-03-07
---

![aniversariantes](/assets/images/aniversariantes.jpeg)

Na última semana, tivemos a comemoração dos aniversariantes do mês. Foram dois irmãos, Paulo Henrique e Lucas, que completaram mais um ano de vida!

<!--more-->

Além dos objetivos pedagógicos, ações que reforçam a importância dos indivíduos em seu meio fortalecem a autoestima e aumentam o carinho entre as crianças e trabalhadores.

A turma se empolgou bastante com  o bolo, que estava delicioso! Parabéns aos aniversariantes de fevereiro!
