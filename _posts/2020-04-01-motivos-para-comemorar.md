---
layout: post
title:  "Motivos para comemorar!"
date:   2020-04-01
---

![cestas](/assets/images/cestas1.jpeg)

Nosso mais profundo agradecimento a todas as pessoas que têm colaborado com nossa campanha!

<!--more-->

Os salários desse mês das nossas colaboradoras estão garantidos.

Além disso, no último sábado, 28/03/2020, entregamos as primeiras 60 cestas básicas compradas com a colaboração de pessoas que decidiram apoiar nossa causa.

São 60 famílias que terão suas dificuldades amenizadas durante o período de isolamento social.

A campanha continua e planejamos nova distribuição de alimentos, à medida que cheguem novas colaborações.

![cestas](/assets/images/cestas2.jpeg)
![cestas](/assets/images/cestas3.jpeg)
![cestas](/assets/images/cestas4.jpeg)
