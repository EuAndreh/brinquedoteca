---
layout: post
title:  "Uhull! Estamos de cara nova."
date:   2019-10-14
---

![Imagem Teste](/assets/images/imagemTeste.jpeg)

Agora, o trabalho social da Casa do Caminho está totalmente renovado! Enfim, agora fomos batizados como Projeto Florescer.

<!--more-->

E para comemorar os 4 anos na ativa, estamos com nova identidade visual e novo site! Agora você pode acompanhar tudo o que está acontecendo no Florescer por aqui! Aproveite para explorar o novo site!

E siga o nosso perfil no Instagram também.
