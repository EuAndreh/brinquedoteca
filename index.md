---
layout: home
---

{: .center}
![logoFlorescer](/assets/images/florescer14.png)

O florescimento simboliza a manifestação das melhores qualidades de um ser. Apropriando-se dessa representação, o Projeto Florescer propõe-se a apoiar o desenvolvimento de crianças e jovens, levando-os a expressar o que há de melhor em si. Essa é a nossa proposta para contribuir com a sociedade e fortalecer o ambiente à nossa volta.
