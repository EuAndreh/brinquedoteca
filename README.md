# brinquedoteca

Conteúdo e código do site do Projeto Florescer.

## Licença

O conteúdo é licensiado sob [CC BY-SA 4.0][cc-by-sa], e o código sob
[AGPLv3][license].

[cc-by-sa]: https://creativecommons.org/licenses/by-sa/4.0/
[license]: ./COPYING
