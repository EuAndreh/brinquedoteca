---
layout: page
title: Sobre
---
![imagemSobre](/assets/images/sobre.jpeg)

O Projeto Florescer tem o objetivo de ampliar os horizontes de crianças e jovens de 6 a 18 anos por meio de uma educação lúdica e transformadora.

Acreditamos que a educação é um poderoso método de empoderamento e desenvolvimento. Assim desejamos colaborar na construção de um mundo melhor.

Começamos em 2015 e, desde então, temos buscado nos aperfeiçoar, aprimorar as nossas instalações, contratar mais e melhores profissionais para melhor atender o nosso público.

Todos os dias, as crianças e jovens que participam do Florescer, no contraturno escolar, são inseridas em atividades de reforço escolar, leitura, judô, artesanato, e jogos. Entre brincar e estudar, as crianças ainda desfrutam de um delicioso lanche!

Para manter toda essa agitação funcionando, contamos com o apoio de três monitores e diversos voluntários, que levam toda a sua animação e entusiasmo para as suas tarefas.

O Projeto Florescer é uma realização das Obras Sociais do <a href="http://casadocaminho.org.br">Centro Espírita Fraternidade Casa do Caminho</a>.
