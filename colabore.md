---
layout: page
title: Colabore
---
Para mantermos tantas atividades bacanas funcionando, precisamos da sua ajuda! Veja como contribuir:

# Adote uma criança
Colabore com R$ 200,00 mensalmente e mantenha uma criança no Florescer!


# Doação avulsa
Você também pode doar qualquer outro valor que puder. Obrigado!


**Banco do Brasil**
Agência: 1226-2
Conta: 109696-6
CNPJ 11.356.848/0001-74
Associação Obras Sociais do Centro Espírita Fraternidade Casa do Caminho

<form action="https://pagseguro.uol.com.br/checkout/v2/donation.html" method="post"><!-- NÃO EDITE OS COMANDOS DAS LINHAS ABAIXO --><br><input name="currency" type="hidden" value="BRL"><br><input name="receiverEmail" type="hidden" value="contato@oscec.org.br"><br><input alt="Pague com PagSeguro - é rápido, grátis e seguro!" name="submit" src="https://p.simg.uol.com.br/out/pagseguro/i/botoes/doacoes/120x53-doar.gif" type="image"></form>

<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top"><input name="cmd" type="hidden" value="_s-xclick"><br><input name="hosted_button_id" type="hidden" value="RBQA6PLCBWNK4"><br><input title="PayPal - The safer, easier way to pay online!" alt="Faça doações com o botão do PayPal" name="submit" src="https://www.paypalobjects.com/pt_BR/BR/i/btn/btn_donateCC_LG.gif" type="image"><br><img style="display: none !important;" src="https://www.paypal.com/pt_BR/i/scr/pixel.gif" alt="" width="1" hidden="" height="1" border="0"></form>
